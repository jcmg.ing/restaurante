<?php namespace App\Models;

use CodeIgniter\Model;

class AuthModel extends Model {

	protected $table      = 'bot_users';
    protected $primaryKey = 'id_user';

    protected $returnType = 'array';

    protected $allowedFields = ['rol_user', 'name_user', 'email_user', 'phone_user', 'password_user', 'date_user', 'bot_user'];

    protected $useTimestamps = false;
    protected $createdField  = 'create_user';
    protected $updatedField  = 'update_user';
    protected $deletedField  = 'deleted_user';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
	
}
?>
