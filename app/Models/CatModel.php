<?php namespace App\Models;

use CodeIgniter\Model;

class CatModel extends Model {

	protected $table      = 'bot_categoria';
    protected $primaryKey = 'id_cat';

    protected $returnType = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['id_cat','nom_cat','desc_cat'];

    protected $useTimestamps = false;
    protected $createdField  = 'create_cat';
    protected $updatedField  = 'update_cat';
    protected $deletedField  = 'deleted_cat';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    
}
?>
