<?php namespace App\Models;

use CodeIgniter\Model;

class BotModel extends Model {

	protected $table      = 'bot_bots';
    protected $primaryKey = 'id_bot';

    protected $returnType = 'array';

    protected $allowedFields = ['user_bot', 'name_bot', 'name_bot', 'lang_bot', 'location_bot', 'area_bot', 'canal_bot', 'use_bot', 'urlfanpage_bot', 'tokenfanpage_bot', 'plan_bot', 'fecha_bot'];

    protected $useTimestamps = false;
    protected $createdField  = 'create_bot';
    protected $updatedField  = 'update_bot';
    protected $deletedField  = 'deleted_bot';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
	
}
?>
