<?php namespace App\Models;

use CodeIgniter\Model;

class EleModel extends Model {

	protected $table      = 'bot_elementos';
    protected $primaryKey = 'id_ele';

    protected $returnType = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['id_ele','nom_ele','desc_ele'];

    protected $useTimestamps = false;
    protected $createdField  = 'create_ele';
    protected $updatedField  = 'update_ele';
    protected $deletedField  = 'deleted_ele';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    
}
?>
