<?php 

namespace App\Controllers\Administrator;

use App\Controllers\BaseController;

class Escritorio extends BaseController{

    public function index(){

        //Iniciamos las sesiones	
        $this->session = \Config\Services::session();	
        
		// Validamos usuario y permisos
		if ( !isset( $this->session->logged_in ) &&  !isset( $this->session->tipo )) {
            //Si no esta las sesiones mandar a login
            return redirect()->to(base_url( 'authuser' ));
		}else{
            //Si no es cliente mandele para login   
            if($this->session->tipo !== 'admin'){
				return redirect()->to(base_url( 'authuser' ));
			}
        }
        
        /*
        echo view('headers/header-auth');
        echo view('admin/auth-login');
        echo view('footers/footer-auth');
        */
    }

}
