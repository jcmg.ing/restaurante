<?php namespace App\Controllers;



class AuthUser extends BaseController{

    public function index(){   
		
        echo view('admin/auth-login'); 
    }

    public function auth_register(){ 
        echo view('admin/auth-register');     
    }
    /*
	|-------------------------------------------------------------------------------
	| Function Login users
	|-------------------------------------------------------------------------------
	*/
	public function auth_login() {
       
		if ($this->request->isAJAX()){

			$objLoad = ( object ) array(
				'validate'	=> false,
				'type'		=> 'Auth-login'
            );
                        
			helper(['form', 'url']);

			if($this->request->getMethod() !== 'post'){
				$this->validate([]);
				echo json_encode( $objLoad );
				die();
			}


			if(!$this->validate([
				'email'    => ['label' => 'Email', 'rules' => 'required'],
				'password' => ['label' => 'Password', 'rules' => 'required']])){
				
				$objLoad->text =  'Error! Campos incorrectos';
				echo json_encode( $objLoad );
				die();

			}else{

				$email = $this->request->getVar('email');
				$password = $this->request->getVar('password');
				$dataUser = validateUser( $email, $password );
				
			
				if($dataUser->validate == true){

					$objLoad->text 		=  'Fue exitosa la validación. Gracias!!';
					$objLoad->validate 	= true;
					$objLoad->rol 		= $dataUser->rol;

				}else{

					$objLoad->text 		=  'Hubo un error, Usuario Incorrecto!!';
					$objLoad->validate 	= false;
				}
				
				echo json_encode( $objLoad );
				die();

            }
		}

	}
	/*
	|-------------------------------------------------------------------------------
	| Function Login users
	|-------------------------------------------------------------------------------
	*/
	public function register_user() {
       
		if ($this->request->isAJAX()){

			$objLoad = ( object ) array(
				'validate'	=> false,
				'type'		=> 'Auth-register'
			);

			helper(['form', 'url']);

			if($this->request->getMethod() !== 'post'){
				$this->validate([]);
				echo json_encode( $objLoad );
				die();
			}

			if(!$this->validate([
				'name' 		=> ['label' => 'inputName', 		'rules' => 'required'],
				'email' 	=> ['label' => 'inputEmail', 		'rules' => 'required'],
				'phone' 	=> ['label' => 'inputPhone', 		'rules' => 'required'],
				'password' 	=> ['label' => 'inputPassword', 	'rules' => 'required'],
				'password2' => ['label' => 'inputConfPassword', 'rules' => 'required|matches[password]']
				])){

				$objLoad->text =  'Error! Campos incorrectos';
				if($this->request->getVar('password') != $this->request->getVar('password2')){
					$objLoad->text =  'Las contraseñas introducidas no coinciden!!';
				}
				
				echo json_encode( $objLoad );
				die();

			}else{

				$name		= $this->request->getVar('name');
				$email		= $this->request->getVar('email');
				$phone		= $this->request->getVar('phone');
				$password	= $this->request->getVar('password');
		
				$dataUser 	= insertUser( $name, $email, $phone, $password );

				if($dataUser == false){

					$objLoad->text 		=  'El email introducido ya existe!!';
					$objLoad->validate 	= false;

				}else{
					
					$objLoad->text 		=  'Se registro tu usuario. Gracias!!';
					$objLoad->validate 	= true;
					$objLoad->rol 		= 'cliente';

				}
				echo json_encode( $objLoad );
				die();

			}
		}

	}


	

}
