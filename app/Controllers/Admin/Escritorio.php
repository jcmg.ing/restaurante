<?php 

namespace App\Controllers\Admin;

use App\Controllers\BaseController;

class Escritorio extends BaseController{
 
    public function index(){
 
 		// Validamos usuario y permisos
		if ( !isset( $this->session->logged_in ) &&  !isset( $this->session->tipo )) {
            //Si no esta las sesiones mandar a login
            return redirect()->to(base_url( 'authuser' ));
		}else{

            //Si no es cliente mandele para login   
            if($this->session->tipo !== 'cliente'){
				return redirect()->to(base_url( 'authuser' ));
            }
            //Obtenemos el id del usuario para verificar si tiene un bot creado
            $idUser = $this->session->idUsuario;
            $vUserBot = vUserBot($idUser);
            //Si no tiene bot creado lo mandamos para el controlador de crear un bot
            if($vUserBot == 0){
                return redirect()->to(base_url( 'createBot' ));
            }
               
        }
    
          
        echo view('admin/escritorio'); 
        
    }

    public function logout_session() {
        $this->session->destroy();
        return redirect()->to(base_url());

	}

}
