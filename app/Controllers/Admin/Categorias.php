<?php 

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\CatModel;

class Categorias extends BaseController{

    public function index(){

        //Iniciamos las sesiones	
        $this->session = \Config\Services::session();	
    
		// Validamos usuario y permisos
		if ( !isset( $this->session->logged_in ) &&  !isset( $this->session->tipo )) {
            //Si no esta las sesiones mandar a login
            return redirect()->to(base_url( 'authuser' ));
		}else{

            //Si no es cliente mandele para login   
            if($this->session->tipo !== 'cliente'){
				return redirect()->to(base_url( 'authuser' ));
            }
            //Obtenemos el id del usuario para verificar si tiene un bot creado
            $idUser = $this->session->idUsuario;
            $vUserBot = vUserBot($idUser);
            //Si no tiene bot creado lo mandamos para el controlador de crear un bot
            if($vUserBot == 0){
                return redirect()->to(base_url( 'createBot' ));
            }
               
        }

        echo view('admin/categorias'); 
        
	}
	
	public function mostrarCategorias(){
		$userModel= new CatModel($db) ; 
		$result["data"] = $userModel->findAll();

		echo json_encode($result);
	}

	public function agregarCategorias(){
		$field = [ 
			'nom_cat'  => $this->request->getVar('txtCategoriaNombre'),
			'desc_cat' => $this->request->getVar('txtCategoriadescripcion')
		];
			
		$userModel= new CatModel($db) ; 
		$result = $userModel->insert($field);  
		$msg['success'] = false;
		$msg['type'] = 'add';
		if($result){
			$msg['success'] = true;
		}
		echo json_encode($msg);
	}

	public function editEmployee(){
		$userModel= new CatModel($db);   
		$id = $this->request->getVar('id'); 
		$result = $userModel->find($id);
		echo json_encode($result);
	}

	public function modificarCategoria(){
		
		$field = [ 
			'nom_cat'  => $this->request->getVar('txtCategoriaNombre'),
			'desc_cat' => $this->request->getVar('txtCategoriadescripcion')
		];
		$userModel= new CatModel($db) ;  
		$id_cat= $this->request->getVar('txtId');
		$result = $userModel->update($id_cat,$field);
		$msg['success'] = false;
		$msg['type'] = 'update';
		if($result){
			$msg['success'] = true;
		}
		echo json_encode($msg); 
	}

  


	public function eliminarCategorias(){
		$userModel= new CatModel($db) ;  
			$id_cat= $this->request->getVar('id');
			$result = $userModel->delete($id_cat);
			
			$msg['success'] = false;
			if($result){
				$msg['success'] = true;
			}
			echo json_encode($msg);


		
	}

 

}
