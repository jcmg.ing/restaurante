<?php 

namespace App\Controllers;


class CreateBot extends BaseController{

    public function index(){  
        echo view('admin/create-bot'); 
    }


	/*
	|-------------------------------------------------------------------------------
	| Function Login users
	|-------------------------------------------------------------------------------
	*/
	public function create() {
       
		if ($this->request->isAJAX()){

			$objLoad = ( object ) array(
				'validate'	=> false,
				'type'		=> 'Create-Bot'
			);

			helper(['form', 'url']);

			if($this->request->getMethod() !== 'post'){
				$this->validate([]);
				echo json_encode( $objLoad );
				die();
			}
 	

			if ( !isset( $this->session->idUsuario ) ) {
				//Si no esta las sesiones mandar a login
				return redirect()->to(base_url( 'authuser' ));
			}else{


				/*SUBIENDO IMAGEN DEL BOT*/
				$file = $this->request->getFile('avatarBot'); //se obtiene la imagen cargada desde el input file
				if (! $file->isValid()) //se valida si se cargó realmente a través de HTTP sin errores
				{
						throw new RuntimeException($file->getErrorString().'('.$file->getError().')');
				}else{
					$file->move(WRITEPATH.'uploads/img_bots');// si se cargo se sube a la carpeta 
				
					/*=================================================
					Obtención de las variables del formulario
					===================================================*/
					$idUser = $this->session->idUsuario;
					//Creamos array para insertar
						$dataForm = array( 
							'user_bot'			=> $idUser
						,	'name_bot' 			=> $this->request->getVar('nameBot')
						,	'lang_bot' 			=> $this->request->getVar('langBot')
						,	'location_bot' 		=> $this->request->getVar('locationBot')
						,	'avatarBot' 		=> $this->request->getVar('avatarBot')
						,	'area_bot' 			=> $this->request->getVar('areaBot')
						,	'canal_bot' 	    => $this->request->getVar('canalBot')
						,   'use_bot'     		=> $this->request->getVar('useBot')
						,   'urlfanpage_bot'    => $this->request->getVar('urlFanPageBot')
						,   'tokenfanpage_bot'  => $this->request->getVar('tokenFanPageBot')
						,   'plan_bot'     		=> $this->request->getVar('planBot')
						,   'fecha_bot'     	=> date( 'Y-m-d H:i:s' )
					);

		
					$createBot 	= insertBot( $idUser, $dataForm );

					if($createBot == false){
						$objLoad->text 		=  'No se puedo crear el bot, Comunicate con nosotros';
						$objLoad->validate 	= false;
					
					}else{
						$objLoad->text 		=  'Tu bot se creo, es probable que aun no este disponible. Nos pondremos en contacto contigo. Gracias!!';
						$objLoad->validate 	= true;
						$objLoad->rol 		= 'cliente';
					
					}
				}
			}

			echo json_encode( $objLoad );
			die();
			
		}

	}

}
