<?php
    
use App\Models\AuthModel;
use App\Models\BotModel;
use App\Models\CatModel;
/*
|-------------------------------------------------------------------------------
| Function validar un usuario y loguearlo
|-------------------------------------------------------------------------------
*/
function validateUser( $email, $password ){

	$userData = new AuthModel($db);

	//Iniciamos las sesiones	
    $session = \Config\Services::session();	
    
    $objLoad = ( object ) array(
        'validate'	=> false,
        'rol'		=> ''
    );
    $response = false;
    //Metodo para verificar si el usuario existe
    $aValidar = array('email_user' => $email, 'password_user' => md5($password));
    $dataElement = $userData->where($aValidar)->findAll( 1, 0 );

    if ( is_array( $dataElement ) && !empty( $dataElement ) ) {

        //Creamos sesion con los datos del cliente
       $login = [
			'usuario'  	=> $dataElement[0]['name_user'],
			'tipo'     	=> $dataElement[0]['rol_user'],
            'logged_in' => TRUE,
            'idUsuario' => $dataElement[0]['id_user'],
            'botExiste' => $dataElement[0]['bot_user']
		];
        
        $session->set($login);
        $objLoad->validate  =  true;
        $objLoad->rol       =  $dataElement[0]['rol_user'];

		return $objLoad;
    }
    return $objLoad;
}
/*
|-------------------------------------------------------------------------------
| Function para insertar un usuario
|-------------------------------------------------------------------------------
*/
function insertUser( $name, $email, $phone, $password ){

	$userData = new AuthModel($db);
	//Iniciamos las sesiones	
	$session = \Config\Services::session();	

    $response = false;
    
    //validamos que no este repetido
    $dataMessage = $userData->where( 'email_user', $email )->findAll();

    if ( is_array( $dataMessage ) && !empty( $dataMessage ) ) {
        
        return $response;
    
    }else{
        //Creamos array para insertar
        $dataForm = array( 
                    'name_user' 	=> $name
                ,	'email_user' 	=> $email
                ,	'phone_user' 	=> $phone
                ,	'password_user' => md5($password)
                ,	'rol_user' 	    => 'cliente'
                ,   'date_user'     => date( 'Y-m-d H:i:s' )
            );
        //Insertamo el array
        $newData = $userData->insert($dataForm);

        //Creamos variables de session
        $login = [
                'usuario'  	=> $name,
                'tipo'     	=> 'cliente',
                'logged_in' => TRUE
        ];
        $session->set($login);

        $response = true;
    }
    
    return $response;
}
/*
|-------------------------------------------------------------------------------
| Function para insertar un usuario
|-------------------------------------------------------------------------------
*/
function insertBot( $idUser, $dataForm ){

    $botData = new BotModel($db);
    $userData = new AuthModel($db);
	//Iniciamos las sesiones	
    $response = false;
    //Creamos el bot
    $newBot = $botData->insert($dataForm);
    
    if($newBot){
        // Get data
        $dataMessage = array( 
            'bot_user'	=> 1
        );
        //Actualizamos el user
        $updateUser = $userData->update($idUser, $dataMessage);	
        if($updateUser ){
            $response = true;
        }
    }

    return $response;

}
/*
|-------------------------------------------------------------------------------
| Function validar un usuario y loguearlo
|-------------------------------------------------------------------------------
*/
function vUserBot( $id ){

    $userData = new AuthModel($db);
    $response = 0;
    //Metodo para verificar si el usuario existe
    $dataElement = $userData->where( 'id_user', $id )->findAll();
    if ( is_array( $dataElement ) && !empty( $dataElement ) ) {
		return $dataElement[0]['bot_user'];
    }
    return 0;
}




/*
|-------------------------------------------------------------------------------
| Function para insertar una CATEGORIA
|-------------------------------------------------------------------------------
*/
function insertCat($dataForm){

    $catData = new CatModel($db);
	
    $msgerror = false;

    //Creamos la categoria
    $newBot = $catData->insert($dataForm);
    
    if($newBot){
        
        $msgerror= $newBot->error();
    }

    return $msgerror;
}