<?= $this->extend('template/tmp_login') ?>


<?= $this->section('page_title') ?> 
REGISTRAR USUARIO
<?= $this->endSection() ?>


<?= $this->section('page_content') ?>    

    <!-- BEGIN: Content-->

    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section class="row flexbox-container">
                    <div class="col-xl-8 col-10 d-flex justify-content-center">
                        <div class="card bg-authentication rounded-0 mb-0">
                            <div class="row m-0">
                                <div class="col-lg-6 d-lg-block d-none text-center align-self-center pl-0 pr-3 py-0">
                                    <img src="<?= base_url(); ?>/theme/img/pages/register.jpg" alt="branding logo">
                                </div>
                                <div class="col-lg-6 col-12 p-0">
                                    <div class="card rounded-0 mb-0 p-2">
                                        <div class="card-header pt-50 pb-1">
                                            <div class="card-title">
                                                <h4 class="mb-0">Crear una cuenta</h4>
                                            </div>
                                        </div>
                                        <p class="px-2">Rellene el siguiente formulario para crear una nueva cuenta.</p>
                                        <div class="card-content">
                                            <div class="card-body pt-0">
                                                <form action="<?= base_url( 'AuthUser/register_user' ) ?>" method="post" class="formRegister">
                                                    <div class="form-label-group">
                                                        <input type="text" id="inputName" name="name" class="form-control" placeholder="Nombre" required>
                                                        <label for="inputName">Nombre</label>
                                                    </div>
                                                    <div class="form-label-group">
                                                        <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email" required>
                                                        <label for="inputEmail">Email</label>
                                                    </div>
                                                    <div class="form-label-group">
                                                        <input type="tel" id="inputPhone" name="phone" class="form-control" placeholder="Telefono" required>
                                                        <label for="inputPhone">Telefono</label>
                                                    </div>
                                                    <div class="form-label-group">
                                                        <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Contraseña" required>
                                                        <label for="inputPassword">Contraseña</label>
                                                    </div>
                                                    <div class="form-label-group">
                                                        <input type="password" id="inputConfPassword" name="password2" class="form-control" placeholder="Confirmar Contraseña" required>
                                                        <label for="inputConfPassword">Confirmar Contraseña</label>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-12">
                                                            <fieldset class="checkbox">
                                                                <div class="vs-checkbox-con vs-checkbox-primary">
                                                                    <input type="checkbox" checked required>
                                                                    <span class="vs-checkbox">
                                                                        <span class="vs-checkbox--check">
                                                                            <i class="vs-icon feather icon-check"></i>
                                                                        </span>
                                                                    </span>
                                                                    <span class=""> Acepto los términos y condiciones.</span>
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                    <a href="<?= base_url('authuser') ?>" class="btn btn-outline-primary float-left btn-inline mb-50">Iniciar sesión</a>
                                                    <button type="button" class="btn btn-primary float-right btn-inline mb-50" onclick="javascript:SendFormLogin( '.formRegister' )">Registrarse</a>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <!-- END: Content-->

    <!-- Start of Bootstrap Toasts -->
    <section id="bootstrap-toasts">
        <div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-delay="3000">
            <div class="toast-header">
                <img src="<?= base_url(); ?>/theme/img/logo/logo.png" class="rounded mr-2" alt="Toast image">
                <strong class="mr-auto">Iniciaste sesión</strong>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="toast-body">
                Fue exitosa la validación. Gracias!!
            </div>
        </div>
        <div class="toast-bs-container">
            <div class="toast-position">
            </div>
        </div>
    </section>
    <!-- End of Bootstrap Toasts -->

    
<?= $this->endSection() ?>