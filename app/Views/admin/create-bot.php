<?= $this->extend('template/tmp_createbot') ?>


<?= $this->section('page_title') ?> 
CREAR BOT
<?= $this->endSection() ?>


<?= $this->section('page_content') ?>     
<?php  
  
 
?>

    <!-- BEGIN: Content-->
    <div class="app-content content1200">
        <div class="content-wrapper">
            
            <div class="content-body content_create_bot">
                <!-- Form wizard with step validation section start -->
                <section id="validation">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <img src="<?= base_url(); ?>/theme/img/logo/logo.png" class="rounded mr-2" alt="Toast image">
                                    <h2 class="card-title">Vamos a crear tu Bot</h2>
                                    <p>Vamos a configurar tu bot(Asistente Virtual), depediendo de los requerimientos que llenas en este formulario.</p>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <form action="<?= base_url( 'createbot/create' ) ?>" class="steps-validation wizard-circle formCreatBot" enctype="multipart/form-data">
                                            <!-- Step 1 -->
                                            <h6><i class="step-icon feather icon-home"></i> Paso 1</h6>
                                            <fieldset>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="nameBot">Nombre del Bot</label>
                                                            <input type="text" class="form-control required" id="nameBot" name="nameBot">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="langBot">Idioma del Bot</label>
                                                            <select class="custom-select form-control required" id="langBot" name="langBot">
                                                                <option value="Español">Español</option>
                                                                <option value="Ingles">Ingles</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="locationBot">País del Bot</label>
                                                            <select class="custom-select form-control required" id="locationBot" name="locationBot">
                                                                <option value="New">New York</option>
                                                                <option value="Amsterdam">Chicago</option>
                                                                <option value="Berlin">San Francisco</option>
                                                                <option value="Frankfurt">Boston</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="avatarBot">Avatar</label>
                                                            <div class="custom-file">
                                                                <input type="file" class="custom-file-input required" id="avatarBot" name="avatarBot">
                                                                <label class="custom-file-label" for="avatarBot">Landing_Mesa de trabajo 1.png</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <!-- Step 2 -->
                                            <h6><i class="step-icon feather icon-briefcase"></i> Paso 2</h6>
                                            <fieldset>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="areaBot">Area del Bot</label>
                                                            <select class="custom-select form-control required" id="areaBot" name="areaBot">
                                                                <option value="Restaurante">Restaurante</option>
                                                                <option value="Salud">Salud</option>
                                                                <option value="Comercio">Comercio</option>
                                                                <option value="Vivienda">Vivienda</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="canalBot">Canal del Bot</label>
                                                            <select class="custom-select form-control required" id="canalBot" name="canalBot">
                                                                <option value="Web">Web</option>
                                                                <option value="Facebook">Facebook</option>
                                                                <option value="WhatsApp">WhatsApp</option>
                                                                <option value="Telegram">Telegram</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="useBot">Uso del Bot</label>
                                                            <textarea name="useBot" id="useBot" rows="4" class="form-control required"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <!-- Step 3 -->
                                            <h6><i class="step-icon feather icon-image"></i> Paso 3</h6>
                                            <!-- Verificamos segun el canal dejar el fieldset correspondiente -->
                                            <fieldset>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <p>Tranquilo si no tienes idea de estos campos lo puedes dejar en blanco.</p>
                                                        </div>
                                                    </div>
                                                    <div id="canal-Facebook" class="col-12 row">
                                                       
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="urlFanPageBot">Url FanPage</label>
                                                                <input type="text" class="form-control" id="urlFanPageBot" name="urlFanPageBot">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="tokenFanPageBot">Token FanPage</label>
                                                                <input type="text" class="form-control" id="tokenFanPageBot" name="tokenFanPageBot">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="card">
                                                            <div class="card-header">
                                                                <h4 class="card-title">Tipo de plan</h4>
                                                            </div>
                                                            <div class="card-content">
                                                                <div class="card-body">
                                                                    <ul class="list-unstyled mb-0">   
                                                                        <li class="d-inline-block mr-2">
                                                                            <fieldset>
                                                                                <div class="vs-radio-con vs-radio-danger">
                                                                                    <input type="radio" name="planBot" value="Basico">
                                                                                    <span class="vs-radio">
                                                                                        <span class="vs-radio--border"></span>
                                                                                        <span class="vs-radio--circle"></span>
                                                                                    </span>
                                                                                    <span class="">Basico</span>
                                                                                </div>
                                                                            </fieldset>
                                                                        </li>
                                                                        <li class="d-inline-block">
                                                                            <fieldset>
                                                                                <div class="vs-radio-con vs-radio-warning">
                                                                                    <input type="radio" name="planBot" checked value="Medio">
                                                                                    <span class="vs-radio">
                                                                                        <span class="vs-radio--border"></span>
                                                                                        <span class="vs-radio--circle"></span>
                                                                                    </span>
                                                                                    <span class="">Medio</span>
                                                                                </div>
                                                                            </fieldset>
                                                                        </li>
                                                                        <li class="d-inline-block mr-2">
                                                                            <fieldset>
                                                                                <div class="vs-radio-con vs-radio-success">
                                                                                    <input type="radio" name="planBot" value="Premium">
                                                                                    <span class="vs-radio">
                                                                                        <span class="vs-radio--border"></span>
                                                                                        <span class="vs-radio--circle"></span>
                                                                                    </span>
                                                                                    <span class="">Premium</span>
                                                                                </div>
                                                                            </fieldset>
                                                                        </li>
                                                                    </ul>
                                                                    <p>Si quieres tener información acerca de nuestro planes puede visitar el siguiente <a href="">link.</a></p>
                                                                    <p>Uno de nuestros colaboradores se pondran en contacto contigo para los ajustes tecnicos que se deban aplicar.</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- Form wizard with step validation section end -->
            </div>
        </div>
    </div>
    <!-- END: Content-->
    
    <!-- Start of Bootstrap Toasts -->
    <section id="bootstrap-toasts">
        <div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-delay="3000">
            <div class="toast-header">
                <img src="<?= base_url(); ?>/theme/img/logo/logo.png" class="rounded mr-2" alt="Toast image">
                <strong class="mr-auto">Iniciaste sesión</strong>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="toast-body">
                Fue exitosa la validación. Gracias!!
            </div>
        </div>
        <div class="toast-bs-container">
            <div class="toast-position">
            </div>
        </div>
    </section>
    <!-- End of Bootstrap Toasts -->


    <?= $this->endSection() ?>