<?= $this->extend('template/tmp_main') ?>


<?= $this->section('page_title') ?> 
CATEGORIAS
<?= $this->endSection() ?>


<?= $this->section('page_content') ?> 
 
  <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">

            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Categorias</h2>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url('admin/escritorio') ?>">Admin</a>
                                    </li>
                                    <li class="breadcrumb-item active">Categorias
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="alert alert-success" style="display: none;">
		
	</div>
 
            <div class="content-body">
                <!-- Data list view starts -->
                <section id="data-thumb-view" class="data-thumb-view-header">
				<button id="btnAdd" class="btn btn-success">Agregar Categoria</button>
				<table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
				<tr>
                                    <th>Id</th> 
                                    <th>Categoria</th>
                                    <th>Descripción</th>
                                    <th>Accion</th>
                                </tr>
                </thead>
                <tbody id="showdata"> 
				</tbody> 
            </table>  
                </section>
    <!-- dataTable ends -->

 




<!-- Modal para registrar y modificar-->
<div id="myModal" class="modal fade text-left"    >
	<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header bg-primary white">
				<h4 class="modal-title" id="myModalLabel33">Inline Login Form </h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form id="myForm" action="" method="post" class="form-horizontal">
			<input type="hidden" name="txtId" value="0">
				<div class="modal-body">
					<label>Nombre: </label>
					<div class="form-group">
						<input type="text" name="txtCategoriaNombre" class="form-control">
					</div>
					<label>Descripción: </label>
					<div class="form-group">
						<textarea class="form-control" name="txtCategoriadescripcion"></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<button type="button" id="btnSave" class="btn btn-primary">Guardar</button>
				</div>
			</form>
		</div>
	</div>
</div>




<!-- Modal ELIMINAR -->
<div class="modal fade text-left"  id="deleteModal"  >
    <div class="modal-dialog   modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger white">
			<input type="hidden" name="txtId" value="0">
                <h5 class="modal-title" id="myModalLabel120">Confirmar Borrado</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Desea Eliminar este Registro? 
            </div>
            <div class="modal-footer">
                <button id="btnDelete" type="button" class="btn btn-outline-danger waves-effect waves-light" data-dismiss="modal">Borrar Registro</button>
                <button type="button" class="btn btn-outline-dark waves-effect waves-light" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>


            </div>
        </div>
    </div>
    <!-- END: Content-->


    <script>
	$(function(){
		showAllEmployee();


		//function
		function showAllEmployee(){

			//idioma o traduccion de los elementos de la tabla
			var idioma_espanol = {
			"sProcessing":     "Procesando...",
						"sLengthMenu":     "Mostrar _MENU_ registros",
						"sZeroRecords":    "No se encontraron resultados",
						"sEmptyTable":     "Ningún dato disponible en esta tabla =(",
						"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
						"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
						"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
						"sInfoPostFix":    "",
						"sSearch":         "Buscar:",
						"sUrl":            "",
						"sInfoThousands":  ",",
						"sLoadingRecords": "Cargando...",
						"oPaginate": {
							"sFirst":    "Primero",
							"sLast":     "Último",
							"sNext":     "Siguiente",
							"sPrevious": "Anterior"
						},
						"oAria": {
							"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
							"sSortDescending": ": Activar para ordenar la columna de manera descendente"
						},
						"buttons": {
							"copy": "Copiar",
							"colvis": "Visibilidad"
						} 
		}
		
		//Ventana MODAL para agregar  registros
		$('#btnAdd').click(function(){
			$('#myModal').modal('show');
			$('#myModal').find('.modal-title').text('Agregar Nueva Categoria');
			$('#myForm').attr('action', '<?php echo base_url() ?>admin/categorias/agregarCategorias');
		});

		//funcion para agregar un registro a la tabla
		$('#btnSave').click(function(){
			var url = $('#myForm').attr('action'); 
			var data = $('#myForm').serialize();

			//validate form
			var categoriaNombre = $('input[name=txtCategoriaNombre]');
			var categoriaDescripcion = $('textarea[name=txtCategoriadescripcion]');
			var result = '';
			if(categoriaNombre.val()==''){
				categoriaNombre.parent().parent().addClass('has-error');
			}else{
				categoriaNombre.parent().parent().removeClass('has-error');
				result +='1';
			}
			if(categoriaDescripcion.val()==''){
				categoriaDescripcion.parent().parent().addClass('has-error');
			}else{
				categoriaDescripcion.parent().parent().removeClass('has-error');
				result +='2';
			}

			if(result=='12'){ 
				$.ajax({
                    type: 'ajax',
					method: 'post',
					async: false,
					url: url,
					data: data,
					dataType: 'json',
					success: function(response){
							$('#myModal').modal('hide');
							$('#myForm')[0].reset();
							if(response.type=='add'){
								var type = 'registrada'
							}else if(response.type=='update'){
								var type ="modificada"
							}
							$('.alert-success').html('Categoria '+type+' satisfactoriamente').fadeIn().delay(4000).fadeOut('slow');
							table.ajax.reload(null, false);  //aqui se recarga la tabla con la nueva informacion
						
					},
					error: function(){
						alert('No se agrego el registro');
					}
				});
			}
		});

		//Envia el id del registro y muestra su informacion en una ventana modal
		$('#showdata').on('click', '.item-edit', function(){
			var id = $(this).attr('data'); 
			$('#myModal').modal('show');
			$('#myModal').find('.modal-title').text('Editar categoria');
			$('#myForm').attr('action', '<?php echo base_url('admin/categorias/modificarCategoria') ?>');
			$.ajax({
				type: 'ajax',
				method: 'get',
				url: '<?php echo base_url('admin/categorias/editEmployee') ?>',
				data: {id: id},
				async: false,
				dataType: 'json',
				success: function(data){
					$('input[name=txtCategoriaNombre]').val(data.nom_cat);
					$('textarea[name=txtCategoriadescripcion]').val(data.desc_cat);
					$('input[name=txtId]').val(data.id_cat); 
				},
				error: function(){
					alert('no se edito el registro');
				}
			});
		});
		 
		//Elimina un registro con ventana modal  
		$('#showdata').on('click', '.item-delete', function(){
			
			var id = $(this).attr('data');
			 
			$('#deleteModal').modal('show');
			//prevent previous handler - unbind()
			$('#btnDelete').unbind().click(function(){
				$.ajax({
					type: 'ajax',
					method: 'get',
					async: false,
					url: '<?php echo base_url() ?>/admin/categorias/eliminarCategorias',
					data:{id:id},
					dataType: 'json',
					success: function(response){
						if(response.success){
							$('#deleteModal').modal('hide');
							$('.alert-success').html('Categoria borrada satisfactoriamente').fadeIn().delay(4000).fadeOut('slow'); 
							table.ajax.reload(null, false); //aqui se recarga la tabla con la nueva informacion
						}else{
							alert('Error');
						}
					},
					error: function(){
						alert('Error Borrando');
					}
				});
			});
		});
 
		//CREA LA TABLA 
		var table =	$('#example').DataTable( { 
				ajax: '<?php echo base_url() ?>/admin/categorias/mostrarCategorias',
				columns: [
					{ data: "id_cat" },
					{ data: "nom_cat" },
					{ data: "desc_cat" },
					{   "data": null,
						render: function(data, type, row){
							return "<button type='button' class='btn btn-primary item-edit' data='"+data['id_cat']+"'><i class='feather icon-edit'></i></button> <button type='button' class='btn btn-danger item-delete' data='"+data['id_cat']+"'><i class='feather icon-trash'></i></button>"
						},
						"targets": -1
					} 
				],
				language: idioma_espanol
			} ); 
			
		} 
		

	});
</script>
 
 
    <?= $this->endSection() ?>