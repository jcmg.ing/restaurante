<?= $this->extend('template/tmp_login') ?>


<?= $this->section('page_title') ?> 
LOGIN
<?= $this->endSection() ?>


<?= $this->section('page_content') ;
 
 
?>    



  <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section class="row flexbox-container">
                    <div class="col-xl-8 col-11 d-flex justify-content-center">
                        <div class="card bg-authentication rounded-0 mb-0">
                            <div class="row m-0">
                                <div class="col-lg-6 d-lg-block d-none text-center align-self-center px-1 py-0">
                                    <img src="<?= base_url(); ?>/theme/img/pages/login.png" alt="branding logo">
                                </div>
                                <div class="col-lg-6 col-12 p-0">
                                    <div class="card rounded-0 mb-0 px-2">
                                        <div class="card-header pb-1">
                                            <div class="card-title">
                                                <h4 class="mb-0">Iniciar sesión</h4>
                                            </div>
                                        </div>

                                        <p class="px-2">Bienvenido, inicie sesión con su cuenta.</p>
                                        <div class="card-content">
                                            <div class="card-body pt-1">
                                                <form action="<?= base_url( 'AuthUser/auth_login' ) ?>" method="post" class="formLogin">
                                                    <fieldset class="form-label-group form-group position-relative has-icon-left">
                                                        <input type="email" class="form-control" id="Email" name="email" placeholder="Email" required>
                                                        <div class="form-control-position">
                                                            <i class="feather icon-user"></i>
                                                        </div>
                                                        <label for="Email">Email</label>
                                                    </fieldset>

                                                    <fieldset class="form-label-group position-relative has-icon-left">
                                                        <input type="password" class="form-control" id="Password" name="password" placeholder="Password" required>
                                                        <div class="form-control-position">
                                                            <i class="feather icon-lock"></i>
                                                        </div>
                                                        <label for="Password">Contraseña</label>
                                                    </fieldset>
                                                    <div class="form-group d-flex justify-content-between align-items-center">
                                                        <div class="text-left">
                                                            <fieldset class="checkbox">
                                                                <div class="vs-checkbox-con vs-checkbox-primary">
                                                                    <input type="checkbox">
                                                                    <span class="vs-checkbox">
                                                                        <span class="vs-checkbox--check">
                                                                            <i class="vs-icon feather icon-check"></i>
                                                                        </span>
                                                                    </span>
                                                                    <span class="">Recuérdame</span>
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                        <div class="text-right"><a href="auth-forgot-password.html" class="card-link">¿Se te olvidó tu contraseña?</a></div>
                                                    </div>
                                                    <a href="<?= base_url('authuser/auth_register') ?>" class="btn btn-outline-primary float-left btn-inline">Registrarse</a>
                                                    <input type="submit" value="Iniciar sesión" id="buttonLogin" class="btn btn-primary float-right btn-inline" onclick="javascript:SendFormLogin( '.formLogin' )"> 
                                                </form>
                                            </div>
                                        </div>
                                        <div class="login-footer">
                                            <div class="divider"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <!-- END: Content-->

    <!-- Start of Bootstrap Toasts -->
    <section id="bootstrap-toasts">
        <div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-delay="3000">
            <div class="toast-header">
                <img src="<?= base_url(); ?>/theme/img/logo/logo.png" class="rounded mr-2" alt="Toast image">
                <strong class="mr-auto">Iniciaste sesión</strong>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="toast-body">
                Fue exitosa la validación. Gracias!!
            </div>
        </div>
        <div class="toast-bs-container">
            <div class="toast-position">
            </div>
        </div>
    </section>
    <!-- End of Bootstrap Toasts -->
 

<?= $this->endSection() ?>