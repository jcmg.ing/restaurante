<!-- BEGIN: Main Menu-->
<div class="main-menu   menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto">
                <a class="navbar-brand" href="<?php echo base_url(); ?>">
                <style>
                .logo{
                    width:40px;
                    height:40px;
                }
                </style> 
                <h2 class="brand-text mb-0">BOT-RESTAURANT</h2>
                </a>
            </li> 
        </ul>
    </div>
        <div class="shadow-bottom"></div>
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation"> 
                
                <li class="nav-item">
                    <a href="<?= base_url('admin/escritorio'); ?>">
                        <i class="feather icon-home"></i>
                        <span class="menu-title" data-i18n="Dashboard">ESCRITORIO</span> 
                    </a> 
                </li>

                <li class=" navigation-header">
                    <span>Elementos o Productos</span>
                </li>

                <li class="nav-item">
                    <a href="<?= base_url('admin/elementos'); ?>">
                        <i class="feather icon-shopping-cart"></i>
                        <span class="menu-title" data-i18n="Elementos">Elementos</span>
                    </a>
                </li> 

                <li class="nav-item">
                    <a href="<?= base_url('admin/categorias'); ?>">
                        <i class="feather icon-tag"></i>
                        <span class="menu-title" data-i18n="Categorias">Categorias</span>
                    </a>
                </li>

                 
            </ul>
        </div>
    </div>
    <!-- END: Main Menu-->