<link rel="apple-touch-icon" href="<?php echo base_url(); ?>/public/vuexy-html-admin/app-assets/images/ico/apple-icon-120.png">
<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>/public/vuexy-html-admin/app-assets/images/ico/favicon.ico">
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.1.1.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    
<!-- BEGIN: Vendor CSS-->
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/theme/vendors/css/vendors.min.css">
<!-- END: Vendor CSS-->

 <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/theme/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/theme/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/theme/css/colors.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/theme/css/components.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/theme/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/theme/css/themes/semi-dark-layout.css">



<?php
$request = \Config\Services::request();

if ($request->uri->getSegment(1)=="authuser" or $request->uri->getSegment(1)==""){
    //echo $request->uri->getSegment(1);
?>

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/theme/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/theme/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/theme/css/pages/authentication.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/theme/css/style.css">
    <!-- END: Custom CSS-->

    <?php
    }
    ?>

<?php 

if ($request->uri->getSegment(1)=="createBot"){
    //echo $request->uri->getSegment(1);
?>


<!-- BEGIN: Vendor CSS   create-->
    <!-- END: Vendor CSS-->

   

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/theme/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/theme/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/theme/css/plugins/forms/wizard.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/theme/css/style.css">
    <!-- END: Custom CSS-->



<?php
    }
    ?>


<?php 

if ($request->uri->getSegment(2)=="elementos"  or $request->uri->getSegment(2)=="categorias"){
    //echo $request->uri->getSegment(1);
?>

    <!-- BEGIN: Vendor CSSsssssssssssssssss-->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/theme/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/theme/vendors/css/file-uploaders/dropzone.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/theme/vendors/css/tables/datatable/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/theme/vendors/css/tables/datatable/extensions/dataTables.checkboxes.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/theme/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/theme/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/theme/css/colors.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/theme/css/components.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/theme/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/theme/css/themes/semi-dark-layout.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/theme/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/theme/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/theme/css/plugins/file-uploaders/dropzone.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/theme/css/pages/data-list-view.css">
    <!-- END: Page CSS-->


<!-- Bootstrap CSS --> 

    <?php
    }
    ?>