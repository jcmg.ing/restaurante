<!DOCTYPE html>
<html lang="es-mx">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?= $this->include('template/__css');?>
    <title><?= $this->renderSection('page_title');?> </title>
</head>


<body class="vertical-layout vertical-menu-modern 2-columns"  > 
 
    <?= $this->include('template/__menuPrincipal');?>

    <?= $this->include('template/__menuLateral');?>

     
            <?= $this->renderSection('page_content');?> 

    <?= $this->include('template/__footer');?>


    <?= $this->include('template/__js');?>

</body>
</html>