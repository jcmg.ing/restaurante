<!DOCTYPE html>
<html lang="es-mx">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">

    <?= $this->include('template/__css');?>
    <title><?= $this->renderSection('page_title');?> </title>
    
</head>
<body class=" creat_bot"   data-col="1-columns" data-layout="dark-layout">

 

    <!-- BEGIN Content--> 
            <?= $this->renderSection('page_content');?> 
    <!-- END Content-->

    <?= $this->include('template/__footer');?>


    <?= $this->include('template/__js');?>
    

</body>
</html>
 