<!-- Navbar Collapsed Starts -->

 
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile-1">
                <ul class="nav navbar-nav bookmark-icons mr-auto float-left">
                    <li class="nav-item mobile-menu d-xl-none mr-auto">
                        <a class="nav-link nav-menu-main hidden-xs menu-toggle is-active" href="#">
                            <i class="ficon feather icon-menu"></i>
                        </a>
                    </li>
                     
                    <li class="nav-item d-none d-lg-block"> 
                        <b> Bienvenido <?php echo session('usuario').'<br>';?> </b>
                    </li>

                </ul>
                <ul class="nav navbar-nav float-right">
                    <li class="nav-item nav-search">
                        <a class="dropdown-item" href="<?php echo base_url(); ?>admin/escritorio/logout_session">
                            <i class="feather icon-power"></i> Cerrar Session
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>


<!-- Navbar Collapse Ends -->