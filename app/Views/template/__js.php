<?php
$request = \Config\Services::request();

if ($request->uri->getSegment(1)!="createBot" or $request->uri->getSegment(1)==""){
    //echo $request->uri->getSegment(1);
?>
    <!-- BEGIN: Vendor JS-->
    <script src="<?= base_url(); ?>/theme/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="<?= base_url(); ?>/theme/js/core/app-menu.js"></script>
    <script src="<?= base_url(); ?>/theme/js/core/app.js"></script>
    <script src="<?= base_url(); ?>/theme/js/scripts/components.js"></script>
    <!-- END: Theme JS-->

    <script src="<?= base_url(); ?>/theme/js/jquery.form.min.js"></script>
    <script src="<?= base_url(); ?>/theme/js/ajax-appLogin.js"></script>

    <!-- BEGIN: Page JS-->
    <!-- END: Page JS-->


    <?php
    }
    ?>

<?php 

if ($request->uri->getSegment(1)=="createBot"){
    //echo $request->uri->getSegment(1);
?>

<!-- BEGIN: Vendor JS-->
<script src="<?= base_url(); ?>/theme/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="<?= base_url(); ?>/theme/vendors/js/extensions/jquery.steps.min.js"></script>
    <script src="<?= base_url(); ?>/theme/vendors/js/forms/validation/jquery.validate.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="<?= base_url(); ?>/theme/js/core/app-menu.js"></script>
    <script src="<?= base_url(); ?>/theme/js/core/app.js"></script>
    <script src="<?= base_url(); ?>/theme/js/scripts/components.js"></script>
    <!-- END: Theme JS-->
    <script src="<?= base_url(); ?>/theme/js/jquery.form.min.js"></script>
    <script src="<?= base_url(); ?>/theme/js/ajax-appLogin.js"></script>
    <!-- BEGIN: Page JS-->
    <script src="<?= base_url(); ?>/theme/js/main-app.js"></script>
    <!-- END: Page JS-->


<?php
    }
    ?>


<?php 

if ($request->uri->getSegment(2)=="elementos" or $request->uri->getSegment(2)=="categorias"){
    //echo $request->uri->getSegment(1);
?>

<!-- BEGIN: Vendor JS-->
<script src="<?= base_url(); ?>/theme/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="<?= base_url(); ?>/theme/vendors/js/extensions/dropzone.min.js"></script>
    <script src="<?= base_url(); ?>/theme/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="<?= base_url(); ?>/theme/vendors/js/tables/datatable/datatables.buttons.min.js"></script>
    <script src="<?= base_url(); ?>/theme/vendors/js/tables/datatable/datatables.bootstrap4.min.js"></script>
    <script src="<?= base_url(); ?>/theme/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
    <script src="<?= base_url(); ?>/theme/vendors/js/tables/datatable/dataTables.select.min.js"></script>
    <script src="<?= base_url(); ?>/theme/vendors/js/tables/datatable/datatables.checkboxes.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="<?= base_url(); ?>/theme/js/core/app-menu.js"></script>
    <script src="<?= base_url(); ?>/theme/js/core/app.js"></script>
    <script src="<?= base_url(); ?>/theme/js/scripts/components.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="<?= base_url(); ?>/theme/js/scripts/ui/data-list-view.js"></script>
    <script src="<?= base_url(); ?>/theme/js/jquery.form.min.js"></script>
    <script src="<?= base_url(); ?>/theme/js/ajax-appLogin.js"></script>


	  <!-- Optional JavaScript -->
      

<?php
    }
    ?>
