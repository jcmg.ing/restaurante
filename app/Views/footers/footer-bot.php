    <!-- BEGIN: Vendor JS-->
    <script src="<?= base_url(); ?>/theme/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="<?= base_url(); ?>/theme/vendors/js/extensions/jquery.steps.min.js"></script>
    <script src="<?= base_url(); ?>/theme/vendors/js/forms/validation/jquery.validate.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="<?= base_url(); ?>/theme/js/core/app-menu.js"></script>
    <script src="<?= base_url(); ?>/theme/js/core/app.js"></script>
    <script src="<?= base_url(); ?>/theme/js/scripts/components.js"></script>
    <!-- END: Theme JS-->
    <script src="<?= base_url(); ?>/theme/js/jquery.form.min.js"></script>
    <script src="<?= base_url(); ?>/theme/js/ajax-appLogin.js"></script>
    <!-- BEGIN: Page JS-->
    <script src="<?= base_url(); ?>/theme/js/main-app.js"></script>
    <!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>