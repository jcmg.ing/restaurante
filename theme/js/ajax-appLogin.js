(function( $ ){

    /*
    ==============================================
    Scripts Ajax to send AJAX action
    ==============================================
    */
	SendFormLogin = function ( theForm ){
		// Action del formulario
		actionform = $( theForm ).attr( 'action' );

		$( "#buttonLogin" ).attr( 'disabled', 'disabled' );

		var options = {
				type: "POST"
			,	url: actionform
			,	dataType: "json"
			,	success: function( msn ){

				$('.toast-body').html(msn.text);

				$('.toast').prependTo('.toast-bs-container .toast-position').toast('show')

				if( msn.validate == true ){
					
					if(msn.rol == 'cliente'){

						setTimeout( function(){
							window.location.href = 'admin/escritorio';
						}, 3000 );

					}
					if(msn.rol == 'admin'){

						setTimeout( function(){
							window.location.href = 'administrator/escritorio';
						}, 3000 );

					}
				}
			}
			,	error: function(msn){
				console.log(msn);
			}
		}
		$( theForm ).ajaxSubmit( options )

		setTimeout( function(){
			$( "#buttonLogin" ).removeAttr( 'disabled' );
		}, 4000 );

		
	}
	// Publicamos la funcion paraque sea visible desde afuera
	this.SendFormLogin;
	
	$( ".formCreatBot" ).submit(function( event ) {
		event.preventDefault();
		actionform = $( this ).attr( 'action' );
		var options = {
				type: "POST"
			,	url: actionform
			,	dataType: "json"
			,	success: function( msn ){
				
				$('.toast-body').html(msn.text);
				$('.toast').prependTo('.toast-bs-container .toast-position').toast('show')

				if( msn.validate == true ){
					setTimeout( function(){
						window.location.href = 'admin/escritorio';
					}, 2000 );
				}
			}
			,	error: function(msn){
				console.log(msn);
			}
		}
		$( this ).ajaxSubmit( options )
	});

	// categoria
	this.SendFormLogin;
	
	$( ".formCreatCat" ).submit(function( event ) {
		event.preventDefault();
		actionform = $( this ).attr( 'action' );
		var options = {
				type: "POST"
			,	url: actionform
			,	dataType: "json"
			,	success: function( msn ){
				
				$('.toast-body').html(msn.text);
				$('.toast').prependTo('.toast-bs-container .toast-position').toast('show')
				if( msn.validate == true ){
					setTimeout( function(){
						window.location.href = 'admin/categorias';
					}, 2000 );
				}
			}
			,	error: function(msn){
				console.log(msn);
			}
		}
		$( this ).ajaxSubmit( options )
	});
	

})( jQuery );

